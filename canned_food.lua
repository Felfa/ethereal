
local S = ethereal.intllib

function register_canned_food(item, name, hp)
	name = name or string.sub(item, 10, -5) -- format: ethereal:*_raw
			
	minetest.register_craftitem("ethereal:" .. name .. "_canned", {
		description = S("Canned " .. name),
		inventory_image = name .. "_canned.png",
		wield_image = name .. "_canned.png",
		on_use = minetest.item_eat(hp, "ethereal:empty_can"),
		groups = {canned_fish = 1, flammable = 2},
	})
	
	minetest.register_craft({
		output = "ethereal:" .. name .. "_canned 3",
		recipe = {
			{"default:tin_ingot", item, "default:tin_ingot"},
			{"default:tin_ingot", "default:tin_ingot", "default:tin_ingot"}
		}
	})
end

-- recycle
minetest.register_craftitem("ethereal:empty_can", {
	description = S("Empty can"),
	inventory_image = "empty_can.png",
	wield_image = "empty_can.png",
	groups = {empty_can = 1},
})

if minetest.get_modpath("technic") then
    minetest.register_on_mods_loaded(function()
        technic.register_recipe("grinding",
        {
            input = {"ethereal:empty_can 3"},
            output = "technic:tin_dust 4", time = 6
        })
    end)
    if minetest.global_exists("unified_inventory") then
        unified_inventory.register_craft({
            type = "grinding",
            items = {"ethereal:empty_can 3"},
            output = "technic:tin_dust 4"
        })
    end
end
